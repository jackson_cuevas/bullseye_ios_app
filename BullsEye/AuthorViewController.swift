//
//  AuthorViewController.swift
//  BullsEye
//
//  Created by Wepsys_dev on 4/15/19.
//  Copyright © 2019 Wepsys_dev. All rights reserved.
//

import UIKit

class AuthorViewController: UIViewController {
    var authorName = "Jackson Cuevas"
    var jobTitle = "Web & App Developer"
    var email = "jackson.cuevas@hotmail.com"
    
    @IBOutlet weak var UIAuthorName: UILabel!
    @IBOutlet weak var UIJobTitle: UILabel!
    @IBOutlet weak var UIEmail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIAuthorName.text = String(authorName)
        UIJobTitle.text = String(jobTitle)
        UIEmail.text = String(email)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func close(){
        dismiss(animated: true, completion: nil)
    }
}
